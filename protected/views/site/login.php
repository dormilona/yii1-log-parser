<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=array(
	'Вход',
);
?>

<h1>Авторизация</h1>

<p>Пожалуйста, авторизуйтесь, используя свой e-mail и пароль</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Поля, отмеченные <span class="required">*</span>, являются обязательными для заполнения</p>

	<div class="row">
            <div class="form-group">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'username'); ?>
            </div>
	</div>

	<div class="row">
            <div class="form-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
            </div>
	</div>

	<div class="row rememberMe">
            <div class="form-group form-check">
		<?php echo $form->checkBox($model, 'rememberMe', array('class' => 'form-check-input')); ?>
		<?php echo $form->label($model, 'rememberMe', array('class' => 'form-check-label')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
            </div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Войти', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
