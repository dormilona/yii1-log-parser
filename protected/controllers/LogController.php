<?php

class LogController extends Controller
{
        public function accessRules()
        {
            return array(
                array('allow', 
                    'actions' => array('parse'),
                    'users' => array('admin'),
                ),
            );
        }

        /**
	 * Парсинг лог-файлов
	 */
        public function actionParse()
	{
            if (!file_exists(ACCESS_LOG_FILE)){
                echo CJSON::encode(['success' => 0, 'status' => 'fail', 'message' => 'Ошибка открытия файла логов.']);
                Yii::app()->end();
            }
            
            $access_log = file_get_contents(ACCESS_LOG_FILE); 
            $pattern = "/([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[^\[]*\[([^\]]*)\][^\"]*\"([^\"]*)\"\s([0-9]*)\s([0-9]*)(.*)/";
            preg_match_all($pattern, $access_log, $records);
            $ip = $records[1];
            $time = $records[2];
            $request = $records[3];
            $status = $records[4];            
            $sucess = 0;
            
            for ($i = 0; $i < count($ip); $i++) {
                $datetime = (new \DateTime($time[$i], new \DateTimeZone('Europe/Moscow')))->format("Y-m-d H:i:s");                
                $log = new Logs();
                $log->ip = $ip[$i];
                $log->access_time = $datetime;
                $log->request = $request[$i];
                $log->status = $status[$i];
                
                if ($log->save()) {
                    $sucess++;
                }
            }
            
            echo CJSON::encode(['success' => $sucess, 'status' => 'success', 'message' => '']);
        }
}
