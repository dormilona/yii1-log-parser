<?php
     
class m200412_230237_create_logs_table extends CDbMigration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `logs` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `ip` varchar(15) NOT NULL,
            `access_time` datetime NOT NULL,
            `request` varchar(255) NOT NULL,
            `status` int(3) NOT NULL,
            PRIMARY KEY (`id`)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        Yii::app()->db->createCommand($sql)->execute();
    }
     
    public function down()
    {
        $sql = "DROP TABLE logs";
        Yii::app()->db->createCommand($sql)->execute();
    }
}
