$(document).ready(function() {
    $('#parse-button').on("click", function() {
        $.ajax({
            url: '/index.php/log/parse/',
            type: 'POST',
            dataType : "json", 
            beforeSend: function(){
                $('#loader').show()
            },
            success: function (data) {
                if (data.status === 'success') {
                    jAlert('Количество добавленных записей: ' + data.success, 'Парсинг лог-файлов');
                } else {
                    jAlert('Произошла ошибка! ' + data.message, 'Парсинг лог-файлов');
                }
            },
            complete: function(){
                $('#loader').hide()
            }
        });

        return false;
    });
});
